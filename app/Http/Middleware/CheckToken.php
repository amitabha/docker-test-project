<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CheckToken {
    public function handle(Request $request, Closure $next)
    {
        if ($request->token) {
            // проверяем токен...
            // если все ок, то пропускаем запрос дальше
            return $next($request);
        }
        return response()->json('', Response::HTTP_FORBIDDEN);
    }
}

