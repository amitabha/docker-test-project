<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateNewsRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'title' => 'required|string|between:3,255',
            'date' => 'required|date|date_format:Y-m-d',
            'content' => 'nullable|array',
            'content.*.content' => 'required|string|between:1,65000',
            'content.*.id' => 'nullable|int'
        ];
    }
}
