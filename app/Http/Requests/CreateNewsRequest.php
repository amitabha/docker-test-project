<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewsRequest extends FormRequest {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'title' => 'required|string|between:3,255',
            'date' => 'required|date|date_format:Y-m-d',
            'content' => 'required|array',
            'content.*' => 'required|string|between:1,65000',
        ];
    }
}

