<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Http\Resources\NewsCollection;
use App\Models\News;
use App\Models\NewsContent;
use App\Http\Resources\News as NewsResource;
use Illuminate\Http\Response;

class NewsController extends Controller {

    public function index() {
        return new NewsCollection(News::all());
    }

    public function show(int $id) {
        return new NewsResource(News::findOrFail($id));
    }

    public function store(CreateNewsRequest $request) {
        $news = News::create($request->only('title', 'date'));

        foreach ($request->input('content') as $content) {
            NewsContent::create([
                'news_id' => $news->id,
                'text' => $content,
            ]);
        }

        return new NewsResource($news);
    }

    public function update(UpdateNewsRequest $request, int $id) {
        if ($news = News::findOrFail($id)) {
            $news->update($request->only('title', 'date'));

            if ($contents = $request->input('content')) {
                foreach ($contents as $content) {
                    if (array_key_exists('id', $content) && $news_content = NewsContent::find($content['id'])) {
                        if ($news_content->news_id === $id) {
                            $news_content->update([
                                'text' => $content['content']
                            ]);
                        }
                    } else {
                        NewsContent::create([
                            'news_id' => $id,
                            'text' => $content['content'],
                        ]);
                    }
                }
            }

            return new NewsResource($news);
        }
    }

    public function destroy(int $id) {
        if ($news = News::findOrFail($id)) {
            $news->delete();
            return response()->json('',Response::HTTP_NO_CONTENT);
        }
    }

}
