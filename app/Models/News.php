<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model {
    use HasFactory;

    protected $fillable = ['title', 'date'];

    protected $casts = [
        'date' => 'datetime:d:m:Y',
    ];

    public function getTitleAttribute($value) {
        return mb_strtoupper($value);
    }

    public function content() {
        return $this->hasMany(
            NewsContent::class,
            'news_id',
            'id'
        );
    }
}


