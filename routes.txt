+--------+-----------+-----------------+--------------+---------------------------------------------+------------+
| Domain | Method    | URI             | Name         | Action                                      | Middleware |
+--------+-----------+-----------------+--------------+---------------------------------------------+------------+
|        | GET|HEAD  | /               |              | Closure                                     | web        |
|        | GET|HEAD  | api/news        | news.index   | App\Http\Controllers\NewsController@index   | api        |
|        | POST      | api/news        | news.store   | App\Http\Controllers\NewsController@store   | api        |
|        | GET|HEAD  | api/news/{news} | news.show    | App\Http\Controllers\NewsController@show    | api        |
|        | PUT|PATCH | api/news/{news} | news.update  | App\Http\Controllers\NewsController@update  | api        |
|        | DELETE    | api/news/{news} | news.destroy | App\Http\Controllers\NewsController@destroy | api        |
|        | GET|HEAD  | api/user        |              | Closure                                     | api        |
|        |           |                 |              |                                             | auth:api   |
+--------+-----------+-----------------+--------------+---------------------------------------------+------------+
